package BuildClass;
import java.util.Scanner;

public class Complex {
    private double real;
    private double imag;
    public Complex() {
        real = 0;
        imag = 0;
    }
    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }
    public void input(Scanner sc) {
        System.out.print("\tEnter the real part of the complex number: ");
        real = sc.nextDouble();
        System.out.print("\tEnter the imaginary part of the complex number: ");
        imagimag = sc.nextDouble();
    }
     public void output() {
        if (nimag < 0) {
            System.out.println(nreal + " - " + Math.abs(nimag) + "*i");
        } else {
            System.out.println(nreal + " + " + nimag + "*i");
        }
    }
    public Complex sum(Complex no2) {
        double nreal = real + no2.real;
        double nimag = imag + no2.imag;
        return new Complex(nreal, nimag);
    }
    public Complex product (Complex no2) {
        double nreal = real * no2.real - imag * no2.imag;
        double nimag = real * no2.imag + no2.real * imag;
        return new Complex(nreal, nimag);
    }
    public Complex devision(Complex no2) {
        double nreal = (real * no2.real + imag * no2.imag) / (no2.real * no2.real + no2.imag * no2.imag);
        double nimag = (no2.real * imag - real * no2.imag) / (no2.real * no2.real + no2.imag * no2.imag);
        return new Complex(nreal, nimag);
    }
}
