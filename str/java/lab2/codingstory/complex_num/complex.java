import java.util.Scanner;
import java.lang.Math;
public class complexcomplex {
    public static void main(String[] args) {
        double a,b,imag1,imag2,real1,real2;
        Scanner sc = new Scanner(System.in);
        System.out.print("\tEnter the first complex number");
        System.out.print("\tEnter the real part of the complex number: ");
        real1 = sc.nextDouble();
        System.out.print("\tEnter the imaginary part of the complex number: ");
        imag1 = sc.nextDouble();
        System.out.print("\tEnter the second complex number");
        System.out.print("\tEnter the real part of the complex number: ");
        real2 = sc.nextDouble();
        System.out.print("\tEnter the imaginary part of the complex number: ");
        imag2 = sc.nextDouble();
        System.out.print("\tWhat do you want to do\n1: total\n2: calculation\n3: the first number divided by the second");
        int yourchoice=sc.nextInt();
        if(yourchoice==1){
            a=real1+real2;
            b=imag1+imag2;
            if(b>=0)
                System.out.print("the result is:"+a+"+"+Math.abs(b)+"*i" );
            else
                System.out.print("the result is:"+a+"-"+Math.abs(b)+"*i" );
        }
        if(yourchoice==2){
            a=real1*real2-imag1*imag2;
            b=real1*imag2+real2*imag1;
            if(b>=0)
                System.out.print("the result is:"+a+"+"+Math.abs(b)+"*i" );
            else
                System.out.print("the result is:"+a+"-"+Math.abs(b)+"*i" );
        }
        if(yourchoice==3){
            a=(real1*real2+imag1*imag2)/(real2*real2+imag2*imag2);
            b=(imag1*real2-real1*imag2)/(real2*real2+imag2*imag2);
            if(b>=0)
                System.out.print("the result is:"+a+"+"+Math.abs(b)+"*i" );
            else
                System.out.print("the result is:"+a+"-"+Math.abs(b)+"*i" );
        }
        else System.out.print("it's not in the show, check it out!!" );
    }
}
